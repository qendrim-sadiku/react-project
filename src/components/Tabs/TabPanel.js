import React from 'react';
import PropTypes from 'prop-types';

function TabPanel({ children, title, className }) {
    return (
        <div title={title} className={`tab-panel tab-panel--animate ${className}`}>
            { children }
        </div>
    );
}

TabPanel.defaultProps = {
    title: '',
    className: ''
};

TabPanel.propTypes = {
    title: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    className: PropTypes.string
};

export default TabPanel;
