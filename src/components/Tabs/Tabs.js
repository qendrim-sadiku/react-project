import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Tabs = ({ children, ...props }) => {
    /**
     * State
     */
    const [activeTab, setActiveTab] = useState(0);

    /**
     * Fill tabs informations
     */
    const tabs = [];
    if (Array.isArray(children)) {
        children.forEach((el, key) => {
            if (el) {
                tabs.push({
                    title: (el.props && el.props.title) || `Tab ${key}`,
                    key
                });
            }
        });
    }

    if (React.isValidElement(children)) {
        tabs.push({
            title: children.props.title || 'Tab 1',
            key: 0
        });
    }

    /**
     * Handlers
     */
    const handleTabClick = (tabIndex) => {
        if (tabIndex === activeTab) return;
        setActiveTab(tabIndex);
        props.handleTabClick(tabIndex);
    };

    const renderTabsHeaderItems = () => (
        tabs.map(element => {
            const isActive = activeTab === element.key;
            return (
                <span
                    key={element.key}
                    tabIndex={element.key}
                    className={`${isActive ? 'active' : ''}`}
                    onClick={() => handleTabClick(element.key)}
                >
                    <li
                        className={`${isActive ? 'font-weight-600  active' : 'font-weight-500'}`}
                    >
                        {element.title}
                    </li>
                </span>
            );
        })
    );

    return (
        <div className={`tabs ${props.className}`}>
            <div className="tabs__header">
                <ul className="list-group list-group-horizontal">
                    { renderTabsHeaderItems() }
                </ul>
            </div>
            <main>
                { Array.isArray(children) && children[activeTab] }
                { React.isValidElement(children) && children }
            </main>
        </div>
    );
};

Tabs.defaultProps = {
    className: '',
    handleTabClick: () => {}
};

Tabs.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.array,
        PropTypes.object
    ]).isRequired,
    className: PropTypes.string,
    handleTabClick: PropTypes.func
};

export default Tabs;
