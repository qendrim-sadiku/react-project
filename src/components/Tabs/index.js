import _Tabs from './Tabs';
import _TabPanel from './TabPanel';

export const Tabs = _Tabs;
export const TabPanel = _TabPanel;

export default Tabs;
