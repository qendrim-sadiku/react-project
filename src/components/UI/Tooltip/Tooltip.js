import React, { useEffect, useState, useRef } from 'react';
import Delete from 'static/icons/active-delete.svg';
import PropTypes from 'prop-types';
const Tooltip = (props) => {
    const node = useRef();
    const [state, setState] = useState(true);

    const handleClick = e => {
        if (node.current.contains(e.target)) {
            // inside click
            return;
        }
        // outside click
        setState(false);
    };

    useEffect(() => {
        document.addEventListener('mousedown', handleClick);

        return () => {
            document.removeEventListener('mousedown', handleClick);
        };
    }, []);

    return (
        <div ref={node}>
            {state && <div>
                <div className="delete-tooltip" onClick={e => {
                    setState(!state);
                    props.onClick();
                }} >
                    <img src={Delete} alt=""/>
                    <span>Delete this Project</span>
                </div>
            </div>}
        </div>
    );
}

Tooltip.defaultProps = {
    className: '',
};

Tooltip.propTypes = {
    onClick: PropTypes.func.isRequired,
}


export default Tooltip;
