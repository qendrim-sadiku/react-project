import React from 'react';
import PropTypes from 'prop-types';

/**
 * Components
 */
import Modal from 'components/UI/Modals/Modal';

const ConfirmDeleteModal = ({
    show,
    title,
    message,
    onClose,
    onConfirm
}) => (
    <Modal
        title={title}
        show={show}
        className="confirm-delete-modal"
        onClose={onClose}
        footer={(
            <div className="delete-modal-footer">
                <span
                    className="btn text-muted"
                    onClick={onClose}
                >
                    Avbryt
                </span>
                <span
                    className="btn btn-danger"
                    onClick={onConfirm}
                >
                    Slettet
                </span>
            </div>
        )}
    >
        <span className="message">{message}</span>
    </Modal>
);

ConfirmDeleteModal.propTypes = {
    show: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired
};

export default ConfirmDeleteModal;
