import React from 'react';
import PropTypes from 'prop-types';
import RemoveIcon from 'static/icons/simple-remove.svg';
/**
 * Components
 */
import BackgroundOverlay from 'components/UI/BackgroundOverlay';
import ComponentSpinner from 'components/UI/ComponentSpinner';

const Modal = ({
                   show,
                   className,
                   onClose,
                   loading,
                   ...props
               }) => (
    <>
        <div
            className={`modal modal-custom ${show ? 'show' : ''} ${className}`}
        >
            <div className="modal-dialog">
                <div className="modal-content">

                    {/* <!-- Modal Header --> */}
                    <div className="modal-header">
                        <h5 className="modal-title font-weight-500">{props.title}</h5>
                        <span
                            className="close"
                            onClick={onClose}
                        >
                        <img src={RemoveIcon} alt=""/>
                        </span>
                    </div>

                    {/* <!-- Modal body --> */}
                    <div className="modal-body">
                        {props.children}
                    </div>

                    {/* <!-- Modal footer --> */}
                    <div className={`modal-footer ${props.footerClassName}`}>
                        {props.footer}
                    </div>
                </div>
                {
                    loading && (
                        <ComponentSpinner background/>
                    )
                }
            </div>
            <div
                className="modal-custom__background-close"
                onClick={onClose}
            />
        </div>

        <BackgroundOverlay
            show={show}
            click={onClose}
            zIndex="1040"
        />
    </>
);

Modal.defaultProps = {
    className: '',
    footer: null,
    footerClassName: '',
    loading: false
};

Modal.propTypes = {
    show: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    className: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    footer: PropTypes.node,
    footerClassName: PropTypes.string,
    // children: PropTypes.node.isRequired,
    loading: PropTypes.bool
};

export default Modal;
