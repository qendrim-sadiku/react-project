import React from 'react';

function PageSpinner() {
    return (
        <div className="page-spinner d-flex justify-content-center align-items-center">
            <div className="spinner-border text-dark">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
}

export default PageSpinner;
