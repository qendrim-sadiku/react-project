import React from 'react';
import PropTypes from 'prop-types';

const ComponentSpinner = ({ background }) => {
    return (
        <div
            className={`component-spinner ${background ? 'component-spinner--background' : ''} d-flex justify-content-center align-items-center`}
        >
            <div className="spinner-border text-dark">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
};

ComponentSpinner.defaultProps = {
    background: false,
};

ComponentSpinner.propTypes = {
    background: PropTypes.bool
};

export default ComponentSpinner;
