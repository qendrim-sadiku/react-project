import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

function BackgroundOverlay({ show, ...props }) {
    useEffect(() => {
        if (show) {
            document.body.style.overflow = 'hidden';
        } else {
            document.body.style.overflow = 'unset';
        }
    }, [show]);

    // conditional style
    const style = {};
    if (props.zIndex !== '') {
        style.zIndex = props.zIndex;
    }

    return (
        <div
            className={`background-overlay ${show ? 'show' : ''}`}
            onClick={() => props.click()}
            style={style}
        />
    );
}

BackgroundOverlay.defaultProps = {
    show: false,
    click: () => (null),
    zIndex: ''
};

BackgroundOverlay.propTypes = {
    show: PropTypes.bool,
    click: PropTypes.func,
    zIndex: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ])
};

export default BackgroundOverlay;
