import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { setupAxios } from 'utils/axios';
import history from 'utils/history';
import App from './App';
import * as serviceWorker from './serviceWorker';

// import store configurator
import configureStore from './store';

// config redux store
const initialState = {};
const store = configureStore(initialState, history);

// configure axios
setupAxios(store);

ReactDOM.render((
    <Provider store={store}>
        <App/>
    </Provider>
), document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
