import { all } from 'redux-saga/effects';
import { watcher as user } from 'sagas/auth/userSaga';
import { watcher as projects } from 'sagas/projects/projects';
import { watcher as project } from 'sagas/project/project';
import { watcher as localizations } from 'sagas/localizations/localization';
import { watcher as projectUser} from 'sagas/user/user';

export default function* root() {
    yield all([
        // auth sagass
        user(),
        projects(),
        project(),
        localizations(),
        projectUser(),
    ]);
}
