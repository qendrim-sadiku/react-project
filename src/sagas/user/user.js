import produce from 'immer';
import { put, takeLatest ,delay} from 'redux-saga/effects';
import createAction from 'utils/action-creators';
import axios from 'utils/axios';


/**
 * Constants
 */
export const REQUEST = 'usersaga/REQUEST';
export const SUCCESS = 'usersaga/SUCCESS';
export const USER_JOIN_REQUEST = 'usersaga/USER_JOIN_REQUEST';
export const USER_JOIN_SUCCESS = 'usersaga/USER_JOIN_SUCCESS';
export const FAILURE = 'usersaga/FAILURE';
export const INVITE_USER_REQUEST = 'usersaga/INVITE_USER_REQUEST';
export const INVITE_USER_SUCCESS = 'usersaga/INVITE_USER_SUCCESS';
export const GET_SUGGESTED_USER_REQUEST = 'usersaga/GET_SUGGESTED_USER_REQUEST';
export const GET_SUGGESTED_USER_SUCCESS = 'usersaga/GET_SUGGESTED_USER_SUCCESS';
export const SET_LOADING = 'usersaga/SET_LOADING';
/**
 * Initial state
 */
const initState = {
    loading: true,
    error: null,
    users: []
};

/**
 * Defualt reducer
 *
 * @param state
 * @param action
 */
const reducer = (state = initState, { payload, ...action }) => (
    produce(state, draft => {
        switch (action.type) {
        case REQUEST:
            draft.item = initState.item;
            draft.loading = true;
            break;

        case SUCCESS:
            draft.item = payload;
            draft.loading = false;
            break;
        case USER_JOIN_REQUEST:
            draft.item = initState.item;
            draft.loading = true;
            break;

        case USER_JOIN_SUCCESS:
            draft.item = payload;
            draft.loading = false;
            break;
        case GET_SUGGESTED_USER_REQUEST:
            draft.users = initState.item;
            draft.loading = true;
            break;
        case GET_SUGGESTED_USER_SUCCESS:
            draft.users = payload;
            console.log(payload);
            break;
        case FAILURE:
            draft.error = payload;
            break;
        default:
            break;
        }
    })
);

/**
 * Selectors
 */

/**
 * Redux actions
 */
export const actions = {
    request: (data) => createAction(REQUEST, data),
    success: (data) => createAction(SUCCESS, data),
    failure: (data) => createAction(FAILURE, data),
    inviteUser: (payload) => createAction(INVITE_USER_REQUEST, payload),
    userJoin: (payload) => createAction(USER_JOIN_REQUEST, payload),
    getSuggestedUsers: (payload) => createAction(GET_SUGGESTED_USER_REQUEST, payload),
};


export default reducer;

/**
 * Sagas
 */
export const sagas = {
    /**
     */* request({ payload }) {
        yield put(createAction(SET_LOADING, true));

        try {
            const response = yield (axios.get(`/project/${payload.project_id}/users`));
            yield put(actions.success(response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message) || 'Error: Something went wrong please contact support!';
            yield put(actions.failure(error));
        }
    },

    * inviteUser({ payload }) {
        try {
            const response = yield axios.post('/user-project/invite', payload);
            yield put(createAction(INVITE_USER_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * getSuggestedUsers({ payload }) {
        yield put(createAction(SET_LOADING, true));

        try {
            yield delay(500);
            const response = yield (axios.get(`/user/suggest/${payload}`));
            console.log(response.data.data);
            yield put(createAction(GET_SUGGESTED_USER_SUCCESS,response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message) || 'Error: Something went wrong please contact support!';
            yield put(actions.failure(error));
        }
    },

    * userJoin({ payload }) {
        yield put(createAction(SET_LOADING, true));

        try {
            const response = yield (axios.post(`/user-project/join`, {
                code: payload.code
            }));
            yield put(actions.success(response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message) || 'Error: Something went wrong please contact support!';
            yield put(actions.failure(error));
        }
    },

};

/**
 * Saga watchers
 */
export const watcher = function* watch() {
    yield takeLatest(REQUEST, sagas.request);
    yield takeLatest(USER_JOIN_REQUEST, sagas.userJoin);
    yield takeLatest(INVITE_USER_REQUEST, sagas.inviteUser);
    yield takeLatest(GET_SUGGESTED_USER_REQUEST, sagas.getSuggestedUsers);
};
