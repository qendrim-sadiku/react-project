import produce from 'immer';
import { put, takeLatest } from 'redux-saga/effects';
import createAction from 'utils/action-creators';
import axios from 'utils/axios';


/**
 * Constants
 */
export const REQUEST = 'projectssaga/REQUEST';
export const ADDPROJECTREQUEST = 'projectssaga/ADDPROJECTREQUEST';
export const SUCCESS = 'projectssaga/SUCCESS';
export const FAILURE = 'projectssaga/FAILURE';
export const SET_LOADING = 'projectssaga/SET_LOADING';
export const ADD_NEW_PROJECT_SUCCESS = 'projectssaga/ADD_NEW_PROJECT_SUCCESS';
/**
 * Initial state
 */
const initState = {
    loading: true,
    errorStatus: '',
    items: [],
    data: {
        localizations:[]
    }
};

/**
 * Defualt reducer
 *
 * @param state
 * @param action
 */
const reducer = (state = initState, { payload, ...action }) => (
    produce(state, draft => {
        switch (action.type) {
        case REQUEST:
            draft.items = payload;
            draft.loading = true;
            break;
        case SUCCESS:
            draft.items = payload;
            draft.loading = false;
            break;
        case ADD_NEW_PROJECT_SUCCESS: {
            const { project } = payload;
            project.localizations = payload.localizations;
            draft.items.push(project);
            break;
        }
        case FAILURE:
            draft.errorStatus = payload;
            break;
        default:
            break;
        }
    })
);

/**
 * Selectors
 */

/**
 * Redux actions
 */
export const actions = {
    request: (data) => createAction(REQUEST, data),
    addnewProjectRequest: (payload) => createAction(ADDPROJECTREQUEST, payload),
    success: (data) => createAction(SUCCESS, data),
    failure: (data) => createAction(FAILURE, data),
};


export default reducer;

/**
 * Sagas
 */
export const sagas = {
    /**
     * Request Saga (payload shoud be as query string)
     */* request() {
        yield put(createAction(SET_LOADING, true));

        try {
            const response = yield (axios.get('/project'));
            // yield put(createAction(SET_DATA, response.data.data));
            yield put(actions.success(response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message);
            yield put(actions.failure(error));
        }

        // yield put(createAction(SET_LOADING, false));
    },

    * addnewProjectRequest({payload}) {
        try {
            const response = yield axios.post('/project', payload);
            yield put(createAction(ADD_NEW_PROJECT_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

};

/**
 * Saga watchers
 */
export const watcher = function* watch() {
    yield takeLatest(REQUEST, sagas.request);
    yield takeLatest(ADDPROJECTREQUEST, sagas.addnewProjectRequest);

};
