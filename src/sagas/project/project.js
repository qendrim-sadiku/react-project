import produce from 'immer';
import { put, takeLatest } from 'redux-saga/effects';
import createAction from 'utils/action-creators';
import axios from 'utils/axios';



/**
 * Constants
 */
export const REQUEST = 'projectsaga/REQUEST';
export const SUCCESS = 'projectsaga/SUCCESS';
export const FAILURE = 'projectsaga/FAILURE';
export const DELETE_PROJECT = 'projectsaga/DELETE_PROJECT';
export const DELETE_PROJECT_SUCCESS = 'projectsaga/DELETE_PROJECT_SUCCESS';
export const DELETE_LOCALIZATION = 'projectsaga/DELETE_LOCALIZATION';
export const DELETE_LOCALIZATION_SUCCESS = 'projectsaga/DELETE_LOCALIZATION_SUCCESS';
export const ADD_LANGUAGE_REQUEST = 'projectsaga/ADD_LANGUAGE_REQUEST'
export const ADD_NEW_LANGUAGE_SUCCESS = 'projectsaga/ADD_NEW_LANGUAGE_SUCCESS';
export const SET_LOADING = 'projectsaga/SET_LOADING';
export const SET_CURRENTLY = 'SET_CURRENTLY';
export const EDIT_LOCALIZATION_VALUE_REQUEST = 'projectsaga/EDIT_LOCALIZATION_VALUE_REQUEST';
export const EDIT_LOCALIZATION_VALUE_SUCCESS = 'projectsaga/EDIT_LOCALIZATION_VALUE_SUCCESS';
/**
 * Initial state
 */
const initState = {
    loading: true,
    errorStatus: '',

    item: {
        localizations: []
    },
    currentEditing: null,
};

/**
 * Defualt reducer
 *
 * @param state
 * @param action
 */
const reducer = (state = initState, { payload, ...action }) => (
    produce(state, draft => {
        switch (action.type) {
        case REQUEST:
            draft.item = initState.item;
            draft.loading = true;
            break;
        case SUCCESS:
            draft.item = payload;
            draft.loading = false;
            break;
        case FAILURE:
            draft.errorStatus = payload;
            break;
        case DELETE_LOCALIZATION_SUCCESS: {
            console.log(payload);
            draft.item.localizations = draft.item.localizations.filter(x => x.id !== payload);
            break;
        }
        case DELETE_PROJECT_SUCCESS: {
            console.log(payload);
            draft.item = draft.item.filter(x => x.id !== payload);
            break;
        }
        case EDIT_LOCALIZATION_VALUE_SUCCESS: {
            const index = draft.item.localizations.findIndex(x => x.id === payload.id);
            console.log(index);
            draft.item.localizations[index] = payload;
            break;
        }
        case ADD_NEW_LANGUAGE_SUCCESS: {
            draft.item.localizations.push(payload);
            break;
        }
        case SET_CURRENTLY: {
            draft.currentEditing = null;
            if (payload) {
                draft.currentEditing = draft.item.localizations.filter(x => x.id === payload)[0];
            }
            break;
        }
        default:
            break;
        }
    })
);

/**
 * Selectors
 */

/**
 * Redux actions
 */
export const actions = {
    request: (data) => createAction(REQUEST, data),
    editLocalizationValue: (payload) => createAction(EDIT_LOCALIZATION_VALUE_REQUEST, payload),
    success: (data) => createAction(SUCCESS, data),
    failure: (data) => createAction(FAILURE, data),
    addnewLanguageRequest: (payload) => createAction(ADD_LANGUAGE_REQUEST, payload),
    deleteLocalization: (payload) => createAction(DELETE_LOCALIZATION, payload),
    deleteProject: (payload) => createAction(DELETE_PROJECT, payload),
    setCurrentlyEditing: (payload) => createAction(SET_CURRENTLY, payload),
};


export default reducer;

/**
 * Sagas
 */
export const sagas = {
    /**
     */* request({ payload }) {
        yield put(createAction(SET_LOADING, true));

        try {
            const response = yield (axios.get(`/project/${payload.project_id}`));
            yield put(actions.success(response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message) || 'Error: Something went wrong please contact support!';
            yield put(actions.failure(error));
        }

        // yield put(createAction(SET_LOADING, false));
    },

    * addnewLanguageRequest({ payload }) {
        try {
            const response = yield axios.post('/localization', payload);
            yield put(createAction(ADD_NEW_LANGUAGE_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * deleteLocalization({ payload }) {
        try {
            yield axios.delete(`/localization/${payload}`);
            // eslint-disable-next-line no-undef
            yield put(createAction(DELETE_LOCALIZATION_SUCCESS, payload));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * deleteProject({ payload }) {
        try {
            yield axios.delete(`/project/${payload.project_id}`);
            // eslint-disable-next-line no-undef
            yield put(createAction(DELETE_PROJECT_SUCCESS, payload));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * editLocalizationValue({ payload }) {
        try {
            // debugger;
            const response = yield axios.patch(`/localization/${payload.id}`, {
                name: payload.name,
                abbreviation: payload.abbreviation,
                project_id: payload.project_id
            });
            console.log(response);
            // eslint-disable-next-line no-undef
            yield put(createAction(EDIT_LOCALIZATION_VALUE_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },
};

/**
 * Saga watchers
 */
export const watcher = function* watch() {
    yield takeLatest(REQUEST, sagas.request);
    yield takeLatest(DELETE_LOCALIZATION, sagas.deleteLocalization);
    yield takeLatest(DELETE_PROJECT, sagas.deleteProject);
    yield takeLatest(ADD_LANGUAGE_REQUEST, sagas.addnewLanguageRequest);
    yield takeLatest(EDIT_LOCALIZATION_VALUE_REQUEST, sagas.editLocalizationValue);
};
