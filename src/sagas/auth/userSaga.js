import produce from 'immer';
import axios from 'utils/axios';
import { /* takeEvery, */ takeLatest, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import createAction from 'utils/action-creators';


/**
 * import outer actions
 */
// action types
export const LOGIN_REQUEST = '@app/auth/login/LOGIN_REQUEST';
export const REGISTER_REQUEST = '@app/auth/login/REGISTER_REQUEST';
export const LOGOUT = '@app/auth/login/LOGOUT';
export const LOGIN = '@app/auth/login/LOGIN';
export const FAILURE = '@app/auth/login/FAILURE';

export const REGISTER = '@app/auth/register/REGISTER';
// export const TOGGLE_FORM_SUBMITTING = '@app/auth/login/TOGGLE_FORM_SUBMITTING';
export const SET_TOKEN = '@app/auth/login/SET_TOKEN';
export const SET_LOGED_IN = '@app/auth/login/SET_LOGED_IN';
export const SET_REGISTRED_IN = '@app/auth/login/SET_REGISTRED_IN';
export const SET_FORM_STATUS = '@app/auth/login/SET_FORM_STATUS';
export const SET_ME = '@app/auth/login/SET_ME';

// initial reducer state
const initState = {
    logedIn: false,
    registerd: false,
    LoginformStatus: null,
    auth_token: null,
    me: null
};

// Reducer
const reducer = (state = initState, { payload, ...action }) => (
    produce(state, draft => {
        switch (action.type) {
        case SET_TOKEN:
            draft.auth_token = payload.auth_token;
            break;
        case SET_REGISTRED_IN:
            draft.registerd = payload;
            break;
        case SET_LOGED_IN:
            draft.logedIn = payload;
            break;
        case SET_ME:
            draft.me = payload;
            break;
        case SET_FORM_STATUS:
            draft.LoginformStatus = payload;
            break;
        default:
            break;
        }
    })
);
export default reducer;

export const actions = {
    setToken: (payload) => createAction(SET_TOKEN, payload),
    registerRequest: (payload) => createAction(REGISTER_REQUEST, payload),
    loginRequest: (payload) => createAction(LOGIN_REQUEST, payload),
    login: (payload) => createAction(LOGIN, payload),
    register: (payload) => createAction(LOGIN, payload),
    logout: () => createAction(LOGOUT),
};

export const sagas = {

    * registerRequest(action) {
        const {
            payload
        } = action;

        try {
            // make login api call
            yield put(createAction(SET_ME, null));
            const response = yield (axios.post('/users/register', payload));
            yield put(createAction(SET_FORM_STATUS, null));
            // setup REGISTER
            yield put(actions.register(response.auth_token));
        } catch ({response}) {
            yield put(createAction(SET_FORM_STATUS, {
                success: false,
                message: response.data && response.data.message
            }));
        }
    },

    // login request saga
    * loginRequest(action) {
        const {
            payload
        } = action;


        try {
            // make login api call
            yield put(createAction(SET_ME, null));
            const response = yield (axios.post('/users/login', payload));
            yield put(createAction(SET_FORM_STATUS, null));
            // setup login
            yield put(actions.login(response.auth_token));
        } catch ({response}) {
            yield put(createAction(SET_FORM_STATUS, {
                success: false,
                message: response.data && response.data.message
            }));
        }
    },

    * logout() {
        // yield put(createAction(TOGGLE_FORM_SUBMITTING, false));
        localStorage.setItem('auth_token', null);
        yield put(actions.setToken(null));
        yield put(createAction(SET_LOGED_IN, false));
        yield put(push('/auth/login'));
        yield put(createAction(SET_ME, null));
    },

    * register(action) {
        localStorage.setItem('token', action.payload);
        yield put(actions.setToken(action.payload));
        yield put(createAction(SET_REGISTRED_IN, true));
    },
    * login(action) {
        localStorage.setItem('token', action.payload);
        yield put(actions.setToken(action.payload));
        yield put(createAction(SET_LOGED_IN, true));
    },
};

export const watcher = function* watch() {
    yield takeLatest(REGISTER_REQUEST, sagas.registerRequest);
    yield takeLatest(LOGIN_REQUEST, sagas.loginRequest);
    yield takeLatest(LOGOUT, sagas.logout);
    yield takeLatest(LOGIN, sagas.login);
    yield takeLatest(REGISTER, sagas.register);
};
