import produce from 'immer';
import { put, takeLatest } from 'redux-saga/effects';
import createAction from 'utils/action-creators';
import axios from 'utils/axios';


/**
 * Constants
 */
export const REQUEST = 'localizationSaga/REQUEST';
export const ADD_NEW_LOCALIZATION_REQUEST = 'localizationSaga/ADD_NEW_LOCALIZATION_REQUEST';
export const EDIT_LOCALIZATION_VALUE_REQUEST = 'localizationSaga/EDIT_LOCALIZATION_VALUE_REQUEST';

// export const DELETE_LOCALIZATION_REQUEST = 'localizationSaga/DELETE_LOCALIZATION_REQUEST',
export const SUCCESS = 'localizationSaga/SUCCESS';
export const FAILURE = 'localizationSaga/FAILURE';
export const SET_LOADING = 'localizationSaga/SET_LOADING';
export const ADD_NEW_LOCALIZATION_SUCCESS = 'localizationSaga/ADD_NEW_LOCALIZATION_SUCCESS';
export const EDIT_LOCALIZATION_VALUE_SUCCESS = 'localizationSaga/EDIT_LOCALIZATION_VALUE_SUCCESS';
export const DELETE_LOCALIZATION_ITEM = 'localizationsaga/DELETE_LOCALIZATION_ITEM';
export const DELETE_LOCALIZATION_ITEM_SUCCESS = 'localizationsaga/DELETE_LOCALIZATION_ITEM_SUCCESS';
export const DELETE_LOCALIZATION = 'localizationsaga/DELETE_LOCALIZATION';
export const DELETE_LOCALIZATION_SUCCESS = 'localizationsaga/DELETE_LOCALIZATION_SUCCESS';
export const SET_CURRENTLY_EDITING = 'SET_CURRENTLY_EDITING';
/**
 * Initial state
 */
const initState = {
    loading: true,
    errorStatus: '',
    items: [],
    currentEditing: null,
};

/**
 * Defualt reducer
 *
 * @param state
 * @param action
 */
const reducer = (state = initState, { payload, ...action }) => (
    produce(state, draft => {
        switch (action.type) {
        case REQUEST:
            draft.items = [];
            draft.loading = true;
            break;
        case SUCCESS:
            draft.items = payload;
            draft.loading = false;
            break;
        case ADD_NEW_LOCALIZATION_SUCCESS: {
            draft.items.push(payload[0]);
            console.log(payload);
            break;
        }
        case FAILURE:
            draft.errorStatus = payload;
            break;
        case EDIT_LOCALIZATION_VALUE_SUCCESS: {
            console.log("'payload'", payload);
            const index = draft.items.findIndex(x => x.id === payload.id);
            console.log(index);
            draft.items[index] = payload;
            break;
        }
        case DELETE_LOCALIZATION_ITEM_SUCCESS: {
            draft.items = draft.items.filter(x => x.id !== payload);
            console.log(draft.items);
            break;
        }
        case SET_CURRENTLY_EDITING: {
            draft.currentEditing = null;
            if (payload) {
                draft.currentEditing = draft.items.filter(x => x.id === payload)[0];
            }
            break;
        }
        default:
            break;
        }
    })
);

/**
 * Selectors
 */

/**
 * Redux actions
 */
export const actions = {
    request: (data) => createAction(REQUEST, data),
    addnewLocalizationRequest: (data) => createAction(ADD_NEW_LOCALIZATION_REQUEST, data),
    editValue: (payload) => createAction(EDIT_LOCALIZATION_VALUE_REQUEST, payload),
    success: (data) => createAction(SUCCESS, data),
    failure: (data) => createAction(FAILURE, data),
    deleteLocalizationItem: (payload) => createAction(DELETE_LOCALIZATION_ITEM, payload),
    setCurrentlyEditing: (payload) => createAction(SET_CURRENTLY_EDITING, payload),
};


export default reducer;

/**
 * Sagas
 */
export const sagas = {
    /**
     * Request Saga (payload should be as query string)
     */* request({ payload }) {
        yield put(createAction(SET_LOADING, true));

        try {
            const response = yield (axios.get(`/localization/${payload.projectId}/${payload.id}`));
            yield put(actions.success(response.data.data));
        } catch (e) {
            console.error(e);
            const error = (e.response && e.response.data && e.response.data.message) || 'Error: Something went wrong please contact support!';
            yield put(actions.failure(error));
        }

        // yield put(createAction(SET_LOADING, false));
    },


    * addnewLocalizationRequest({ payload }) {
        try {
            const response = yield axios.post('/localization-item', payload);
            console.log(response);
            // eslint-disable-next-line no-undef
            yield put(createAction(ADD_NEW_LOCALIZATION_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * editValue({ payload }) {
        try {
            const response = yield axios.post(`/localization-item/value/${payload.id}`, { value: payload.value });
            console.log(response);
            // eslint-disable-next-line no-undef
            yield put(createAction(EDIT_LOCALIZATION_VALUE_SUCCESS, response.data.data));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },

    * deleteLocalizationItem({ payload }) {
        try {
            yield axios.delete(`/localization-item/${payload}`);
            // eslint-disable-next-line no-undef
            yield put(createAction(DELETE_LOCALIZATION_ITEM_SUCCESS, payload));
            // eslint-disable-next-line no-empty
        } catch ({ response }) {
        }
    },
};

/**
 * Saga watchers
 */
export const watcher = function* watch() {
    yield takeLatest(REQUEST, sagas.request);
    yield takeLatest(ADD_NEW_LOCALIZATION_REQUEST, sagas.addnewLocalizationRequest);
    yield takeLatest(EDIT_LOCALIZATION_VALUE_REQUEST, sagas.editValue);
    yield takeLatest(DELETE_LOCALIZATION_ITEM, sagas.deleteLocalizationItem);
};
