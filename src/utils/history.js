import { createBrowserHistory } from 'history';

// create browser history
const history = createBrowserHistory();

export default history;
