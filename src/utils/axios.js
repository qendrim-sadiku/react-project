import axios from 'axios';
import { API_URL } from 'config/index';

// import actions
import { actions as userActions } from 'sagas/auth/userSaga';

// import history
import history from 'utils/history';

// configure axios defaults
axios.defaults.baseURL = 'https://flowrspot-api.herokuapp.com/api/v1';
axios.defaults.config = {
    headers: { 'Content-Type': 'application/json' }
};

export const setupAxios = (store) => {
    // get token
    const token = window.localStorage.getItem('auth_token');

    // if token exists set token
    // dispatch login action
    if (token && token !== '' && token !== 'null') {
        store.dispatch(userActions.login(token));
    }

    // reqeust intercpetor
    axios.interceptors.request.use(
        config => {
            // config token as default per request
            const auth_token = window.localStorage.getItem('auth_token');
            if (auth_token) {
                config.headers.Authorization = `Bearer ${auth_token}`;
            }
            return config;
        },
    );

    // Add a response interceptor
    axios.interceptors.response.use(
        (response) => {
            // if error: "No permission!"
            // logout & go to login page
            if (response && response.data && response.data.error === 'No permission!') {
                store.dispatch(userActions.logout());
            }

            return response;
        },
        (error) => {
            const _error = { ...error };

            if (_error.response && _error.response.status === 401) {
                // Unauthorized Request
                store.dispatch(userActions.logout());
            } else if (_error.response && _error.response.status === 403) {
                // Forbiden Request
                // store.dispatch(forbidenRequest());
            } else if (_error.response && _error.response.status === 404) {
                // route not found
                // store.dispatch(erroredRequest());
                // history.push('/error/404');
            }

            return Promise.reject(_error);
        }
    );
};

export default axios;
