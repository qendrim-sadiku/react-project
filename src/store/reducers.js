import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

// import app router history
import history from '../utils/history';

// auth reducres
import user from 'sagas/auth/userSaga';
import projects from 'sagas/projects/projects';
import project from 'sagas/project/project';
import projectUser from 'sagas/user/user';
import localizations from 'sagas/localizations/localization';

export default function createReducer(injectedReducers = {}) {
    const rootReducer = combineReducers({
        app: combineReducers(
            {
                user,
                projects,
                project,
                localizations,
                projectUser,
            }
        ),
        ...injectedReducers,
    });

    // Wrap the root reducer and return a new root reducer with router state
    const mergeWithRouterState = connectRouter(history);
    return mergeWithRouterState(rootReducer);
}
