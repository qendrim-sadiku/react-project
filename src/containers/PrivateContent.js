import React from 'react';
import PropTypes from 'prop-types';
import {
    Redirect,
    withRouter
} from 'react-router-dom';
import { connect } from 'react-redux';

function PrivateContent(props) {
    const {
        logedIn,
        children,
    } = props;


    // if not loged in
    if (!logedIn) {
        return (
            <Redirect to="/auth/login" />
        );
    }


    // if everythign ok return children
    return (
        children
    );
}

PrivateContent.defaultProps = {
    me: null
};

PrivateContent.propTypes = {
    children: PropTypes.element.isRequired,
    logedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
    logedIn: state.app.user.logedIn,
});

const connectedPrivateContent = connect(mapStateToProps)(PrivateContent);

export default withRouter(connectedPrivateContent);
