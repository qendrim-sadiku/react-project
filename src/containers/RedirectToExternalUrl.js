import PropTypes from 'prop-types';
import React from 'react';

/**
 * Components
 */
const RedirectToExternal = ({
    to, history, target, goBack
}) => {
    const a = document.createElement('a');
    a.href = to;
    a.target = target;
    a.click();

    if (goBack) {
        history.goBack();
    }
    return (<></>);
};

RedirectToExternal.defaultProps = {
    target: '_blank',
    goBack: false
};

RedirectToExternal.propTypes = {
    to: PropTypes.string.isRequired,
    history: PropTypes.object.isRequired,
    target: PropTypes.string,
    goBack: PropTypes.bool
};

export default RedirectToExternal;
