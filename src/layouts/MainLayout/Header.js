import React from 'react';
import HeaderLogo from 'static/icons/group.svg';
import Circle from 'static/icons/circle-out.svg';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

/**
 * Actions
 */
import { actions as userActions } from 'sagas/auth/userSaga';


const Header = (props) => {
    const {
        logedIn,
        dispatch
    } = props;

    /**
     * Handlers
     */
    const handleLogout = (e) => {
        e.preventDefault();
        dispatch(userActions.logout());
    };


    return (
        <header>
            <div className="container">
                <div className="header-content">
                    <nav className="navbar navbar-light ">
                        <Link to="/projects/list" className="navbar-brand">
                            <img src={HeaderLogo} alt="headerlogo"/>
                        </Link>
                        {logedIn &&
                        <div className="log-out" onClick={handleLogout}>
                            <img src={Circle} alt="circle"/>
                            <span>Log out</span>
                        </div>
                        }
                    </nav>
                </div>
            </div>
        </header>
    );
};

Header.propTypes = {
    logedIn: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
    logedIn: state.app.user.logedIn,
});


export default connect(mapStateToProps)(Header);
