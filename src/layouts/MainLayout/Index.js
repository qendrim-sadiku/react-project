import React from 'react';
import PropTypes from 'prop-types';
import Header from "layouts/MainLayout/Header";

function Index(props) {
    return (
        <>
            <Header />
            <div className="body">
                {props.children}
            </div>
        </>
    );
}

Index.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element
    ]).isRequired
};

export default Index;
