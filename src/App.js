// import modules
import React from 'react';
import {
    Router, Route, Switch
} from 'react-router-dom';
import history from 'utils/history';
import Layout from 'layouts/MainLayout/Index.js';
import LoginPage from './pages/auth/LoginPage';
// Import styles
import 'styles/main.scss';
// import ProjectsPage from "pages/projects/ProjectsPage";
import RegisterPage from "pages/auth/RegisterPage";
import ProjectsPage from "pages/projects/ProjectsPage";
import LocalizationPage from "pages/localization/LocalizationPage";
import PrivateContent from 'containers/PrivateContent';
import ProjectSettings from "pages/project-settings/ProjectSettings";
import UserJoin from "pages/project-settings/users/user_join/UserJoin";

function App() {
    return (
        <div>
            <Router history={history}>
                <Layout>
                    <Switch>
                        {/* Account Routes */}
                        <Route path="/auth">
                            <Switch>
                                <Route exact path="/auth/login" component={LoginPage}/>
                                <Route exact path="/auth/register" component={RegisterPage}/>
                            </Switch>
                        </Route>
                        Main Routes
                        {/*<Route path="/*">*/}
                            <PrivateContent>
                                <Switch>
                                    <Route exact path="/user-project/join" component={UserJoin} />
                                    <Route exact path="/projects/list" component={ProjectsPage} />
                                    <Route exact path="/projects/:project_id/localization/:id" component={LocalizationPage}/>
                                    <Route exact path="/project/settings/:project_id" component={ProjectSettings} />
                                    <Route path="*" component={ProjectsPage} />

                                </Switch>
                            </PrivateContent>
                        {/*</Route>*/}
                    </Switch>
                </Layout>
            </Router>
        </div>
    );
}

export default App;
