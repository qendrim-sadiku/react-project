import React from 'react';
import useForm from 'react-hook-form';
import Modal from 'components/UI/Modals/Modal';
import * as yup from 'yup';
import PropTypes from 'prop-types';

function EditForm({
    currentEditing,
    handleEditModalClose,
    onSubmit
}) {
    const EditFormValidation = yup.object().shape({
        value: yup.string().required('Value is Required'),
    });

    const { handleSubmit, register, errors } = useForm({
        validationSchema: EditFormValidation
    });

    return (
        <>
            <Modal
                title="Add new Localization"
                show
                onClose={handleEditModalClose}
            >
                {currentEditing
            && (
                <form onSubmit={handleSubmit((values) => {
                    const { id } = currentEditing;
                    const { value } = values;
                    onSubmit({ value, id });
                })}
                >
                    <div className="form-wrapper">
                        <label htmlFor="value">Value</label>
                        <input
                            id="value"
                            name="value"
                            ref={register}
                            defaultValue={currentEditing.value}
                        />
                        {errors.value && errors.value.message}
                    </div>
                    <div className="button-part">
                        <button
                            className="btn text-muted"
                            onClick={handleEditModalClose}
                        >
                            Close
                        </button>
                        <button type="submit" className="base-button">Add new Projcet</button>
                    </div>
                </form>
            )}
            </Modal>
        </>
    );
}

EditForm.propTypes = {
    currentEditing: PropTypes.object.isRequired,
    handleEditModalClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};



export default EditForm;
