import React, {useEffect, useState} from 'react';
import Translation from 'static/icons/translation.svg';
import Modal from 'components/UI/Modals/Modal';
import CircleLogo from 'static/icons/circle-add.svg';
import Gear from 'static/icons/settings-gear-63.svg';
import Tabs, {TabPanel} from 'components/Tabs';
import LocalizationItems from 'pages/localization/LocalizationItems';
import {actions as localizationActions} from 'sagas/localizations/localization';
import {actions as projectActions} from 'sagas/project/project';
import {connect} from 'react-redux';
import useForm from 'react-hook-form';
import ComponentSpinner from 'components/UI/ComponentSpinner';
import ConfirmDeleteModal from 'components/UI/Modals/ConfirmDeleteModal';
import EditForm from "pages/localization/EditForm";


function LocalizationPage(props) {
    const {
        dispatch,
        project,
        localizations,
        history,
        error
    } = props;

    const {params} = props.match;

    useEffect(() => {
        // request data first load
        dispatch(
            localizationActions.request({projectId: params.project_id, id: params.id}),
        );
        dispatch(
            projectActions.request(params),
        );
    }, [dispatch, params]);


    /**
     * Sate
     */
    const [showAddModal, setShowAddModal] = useState(false);
    const [showEditModal, setshowEditModal] = useState(false);
    const [confirmDelete, setConfirmDelete] = useState({
        showModal: false,
        confirmed: false,
        id: null
    });
    const [confirmEdit, setConfirmEdit] = useState({
        showModal: false,
        confirmed: false,
        id: null
    });
    /**
     * Handlers
     */
    const handleEditModalClick = () => {
        setshowEditModal(true);
    };

    const handleEditModalClose = () => {
        setshowEditModal(false);
    };

    const handleAddClick = () => {
        setShowAddModal(true);
    };


    const handleAddModalClose = () => {
        setShowAddModal(false);
    };

    const handleOnDelete = (id) => {
        setConfirmDelete({
            showModal: true,
            id
        });
    };

    const handleOnEdit = (id) => {
        dispatch(
            localizationActions.setCurrentlyEditing(id),
        );
        handleEditModalClick();

        setConfirmEdit({
            id
        });
    };

    const handleConfirmDeleteModalConfirm = () => {
        dispatch(localizationActions.deleteLocalizationItem(confirmDelete.id));
        setConfirmDelete({
            showModal: false,
            id: null
        });
    };

    const handleConfirmDeleteModalClose = () => {
        setConfirmDelete({
            showModal: false,
            id: null
        });
    };


    const {handleSubmit, register} = useForm({
        // validationSchema: ProjectsPage
    });
    //
    const onSubmit = (values, e) => {
        dispatch(localizationActions.addnewLocalizationRequest({...values, localization_id: params.id}));
        e.target.reset();
        console.log(values);
        handleAddModalClose();
    };


    const handleDirection = () => {
        history.push('/project/settings/' + project.id);
    };

    const renderFormModal = () => (
        <Modal
            title="Add new Localization"
            show={showAddModal}
            onClose={handleAddModalClose}
        >

            {/* Modal Body */}
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-wrapper">
                    <label htmlFor="key">Key</label>
                    <input
                        name="key"
                        id="key"
                        ref={register}

                    />
                </div>
                <div className="form-wrapper">
                    <label htmlFor="value">Value</label>
                    <textarea
                        id="value"
                        name="value"
                        ref={register}
                    />
                </div>
                <div className="button-part">
                    <button
                        className="btn text-muted"
                        onClick={handleEditModalClose}
                    >
                        Close
                    </button>
                    <button type="submit" className="base-button">Add new Localization</button>
                </div>
            </form>
        </Modal>
    );

    const renderEditFormModal = () => (
        <EditForm
            currentEditing={props.currentEditing}
            handleEditModalClose={handleEditModalClose}
            onSubmit={(data) => {
                dispatch(localizationActions.editValue(data));
                dispatch(
                    localizationActions.setCurrentlyEditing(null),
                );
                setshowEditModal(false);
            }}
        />
    );


    const renderContent = () => (
        <ConfirmDeleteModal
            show={confirmDelete.showModal}
            title="Delete"
            message="Do you really want to delete these records? This process cannot be undone."
            onConfirm={handleConfirmDeleteModalConfirm}
            onClose={handleConfirmDeleteModalClose}
        />
    );

    return (
        <>
            <div className="container localization">
                <div className="content-head  -margin-top-content">
                    <span>
Projects /
                        {project.name}
                    </span>
                    <h4>{project.name}</h4>
                    {localizations && (
                        <div className="transparent-button" onClick={handleDirection}>
                            <img src={Gear} alt=""/>
                            {' '}
                            <p> Project settings </p>
                        </div>
                    )}

                </div>

                <Tabs
                    className=""
                    handleTabClick={(index) => {
                        console.log(localizations);
                        dispatch(
                            localizationActions.request({
                                projectId: params.project_id,
                                id: project.localizations[index].id
                            }),
                        );
                    }}
                >
                    {
                        project.localizations.map((item) => (
                            <TabPanel
                                className=""
                                key={item.id}
                                title={item.name}
                            > {localizations.length >= 1
                            && (
                                <div className="localization-head">


                                    <h5>Localization</h5>

                                    <div className="add-other-language" onClick={handleAddClick}>
                                        <img src={CircleLogo} alt=""/>
                                        Add another Localization
                                    </div>


                                </div>
                            )}
                                <div className="row justify-content-md-center">
                                    <div className="col-md-12 row-centered ">
                                        {!props.loading && localizations.length === 0 && (
                                            <div className="card  content-card">
                                                <div className="content">
                                                    <img src={Translation} alt=""/>
                                                    <h5>Localization</h5>
                                                    <p>To add a new localization to your project, click button below</p>
                                                    <button
                                                        type="submit"
                                                        className="base-button"
                                                        onClick={handleAddClick}
                                                    >
                                                        + Add
                                                        new
                                                        Project
                                                    </button>
                                                </div>
                                            </div>
                                        )}
                                        {props.loading && <ComponentSpinner/>}
                                        {error}
                                        {localizations && (
                                            <div className="project-list">
                                                <LocalizationItems
                                                    onEdit={handleOnEdit}
                                                    onDelete={handleOnDelete}
                                                    localizations={localizations}
                                                />
                                            </div>
                                        )}

                                    </div>
                                </div>
                            </TabPanel>
                        ))
                    }
                </Tabs>
            </div>
            {renderFormModal()}
            {renderContent()}
            {showEditModal && renderEditFormModal(props)}

        </>
    );
}

const mapStateToProps = (state) => ({
    localizations: state.app.localizations.items,
    error: state.app.localizations.error,
    project: state.app.project.item,
    loading: state.app.localizations.loading,
    currentEditing: state.app.localizations.currentEditing
});

export default connect(mapStateToProps)(LocalizationPage);
