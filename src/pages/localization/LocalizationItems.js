import React from 'react';

function LocalizationItems(props) {

    return (
        <>

            <table className="table custom-table">
                <thead>
                <tr>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    props.localizations.map((item) => (
                        <tr>
                            <td>{item.key} </td>
                            <td>{item.value}</td>
                            <td>
                    <span className="edit-icon tooltip" onClick={() => props.onEdit(item.id)}>
                        <svg className="edit-button" xmlns="http://www.w3.org/2000/svg"
                             width="20" height="20" viewBox="0 0 20 20">
                            <g fill="none" fill-rule="evenodd" stroke="#8492A6"
                               stroke-linecap="round" stroke-linejoin="round"
                               stroke-width="1.6"
                            >
                                <path d="M7.3 3.7H1V19h15.3v-6.3"/>
                                <path d="M9.1 13.6l-3.6.9.9-3.6L16.3 1 19 3.7z"/>
                            </g>
                        </svg>
                         <span className="tooltiptext">Delete</span>
                    </span>
                                {/* eslint-disable-next-line react/prop-types */}
                                <span className="delete-icon tooltip" onClick={() => props.onDelete(item.id)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 viewBox="0 0 20 20">
                                <g fill="none" fill-rule="evenodd" stroke="#8492A6"
                                   stroke-linecap="round" stroke-linejoin="round"
                                   stroke-width="1.6">
                                    <path
                                        d="M16.545 7.545v9.819c0 .903-.732 1.636-1.636 1.636H5.091a1.636 1.636 0 0 1-1.636-1.636V7.545M1 4.273h18M10 10v4.91M6.727 10v4.91M13.273 10v4.91M6.727 4.273V1h6.546v3.273"/>
                                </g>
                            </svg>
                            <span className="tooltiptext">Delete</span>
                        </span>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
        </>
    )
}

export default LocalizationItems;
