import React from 'react';
import User from 'static/icons/user.svg';

function ProjectSettingUser(props) {
    return (
        <>
            {
                props.projectUser.map((item) => (
                    <div className="custom-card">
                        <div className="card">
                         <h2>{item.error}</h2>
                            <div className="settings-content">
                                <div className="left-part">
                                    <img src={User} alt=""/>
                                    <h5>{item.email}</h5>
                                </div>
                                <div className="right-part">
                                    <span className="delete-icon tooltip" onClick={() => props.onDelete(item.id)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                         viewBox="0 0 20 20">
                                        <g fill="none" fill-rule="evenodd" stroke="#8492A6"
                                           stroke-linecap="round" stroke-linejoin="round"
                                           stroke-width="1.6">
                                            <path
                                                d="M16.545 7.545v9.819c0 .903-.732 1.636-1.636 1.636H5.091a1.636 1.636 0 0 1-1.636-1.636V7.545M1 4.273h18M10 10v4.91M6.727 10v4.91M13.273 10v4.91M6.727 4.273V1h6.546v3.273"/>
                                        </g>
                                    </svg>
                                    <span className="tooltiptext">Delete</span>
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </>
    )
}

export default ProjectSettingUser;
