import React, {useEffect, useRef, useState} from 'react';
import Translation from 'static/icons/translation.svg';
import Modal from 'components/UI/Modals/Modal';
import CircleLogo from 'static/icons/circle-add.svg';
import Tabs, {TabPanel} from 'components/Tabs';
import {actions as projectActions} from 'sagas/project/project';
import {actions as userActions} from 'sagas/user/user';
import {connect} from 'react-redux';
import useForm from 'react-hook-form';
import ComponentSpinner from 'components/UI/ComponentSpinner';
import ConfirmDeleteModal from 'components/UI/Modals/ConfirmDeleteModal';
import ProjectSettingsItem from "pages/project-settings/project-settings-item";
import ProjectSettingUser from "pages/project-settings/project-settings-user";
import ProjectEditForm from "pages/project-settings/ProjectEditForm";
import * as yup from "yup";
import Tooltip from "components/UI/Tooltip/Tooltip";
import InviteUser from "pages/project-settings/users/InviteUser";

function ProjectSettingsPage(props) {
    const {
        dispatch,
        project,
        projectUser,
        history,
        suggestedUsers,
        error,
    } = props;


    const ProjectSettingsValidation = yup.object().shape({
        name: yup.string().required('Value is Required'),
        abbreviation: yup.string().required('Value is Required'),
    });


    const node = useRef();

    const {params} = props.match;

    let user_id;

    useEffect(() => {
        // request data first load
        dispatch(
            projectActions.request(params),
        );
        dispatch(
            userActions.request(params),
        );
    }, [dispatch, params]);
    /**
     * Sate
     */
    const [showAddModal, setShowAddModal] = useState(false);
    const [showEditModal, setshowEditModal] = useState(false);
    const [showInviteUserModal, setshowInviteUserModal] = useState(false);
    const [confirmDelete, setConfirmDelete] = useState({
        showModal: false,
        confirmed: false,
        id: null
    });
    const [confirmUserDelete, setConfirmUserDelete] = useState({
        showUserModal: false,
        confirmed: false,
        id: null
    });

    const [confirmEdit, setConfirmEdit] = useState({
        showModal: false,
        confirmed: false,
        id: null
    });

    const [state, setState] = useState(false);
    /**
     * Handlers
     */



    const handleEditModalClick = () => {
        setshowEditModal(true);
    };

    const handleEditModalClose = () => {
        setshowEditModal(false);
    };

    const handleInviteUserModalClick = () => {
        setshowInviteUserModal(true);
    };

    const handleInviteUserModalClose = () => {
        setshowInviteUserModal(false);
    };

    const handleAddClick = () => {
        setShowAddModal(true);
    };


    const handleInputChange = (e) => {
        // dispatch
        if (e.target.value) {
            dispatch(userActions.getSuggestedUsers(e.target.value));
        } else if (e.target.value === '') {
            dispatch(userActions.getSuggestedUsers());
        }
    };

    const handleUserSelect = (userId) => {
        user_id = userId;
        console.log(user_id);
        // dispatch(userActions.inviteUser({user_id:user_id, project_id: params.project_id}))
    };

    const handleInviteUser = () => {
        dispatch(userActions.inviteUser({user_id, project_id: params.project_id}))
    };

    const handleAddModalClose = () => {
        setShowAddModal(false);
    };

    const handleOnDelete = (id) => {
        setConfirmDelete({
            showModal: true,
            id
        });
    };

    const handleOnUserDelete = (id) => {
        alert(id);
        setConfirmUserDelete({
            showUserModal: true,
            id
        });
    };

    const handleOnEdit = (id) => {
        dispatch(
            projectActions.setCurrentlyEditing(id),
        );
        handleEditModalClick();
        setConfirmEdit({
            id
        });

    };


    const handleProjectDelete = () => {
        dispatch(projectActions.deleteProject(params));
        history.push('/project/list');
    };

    const handleConfirmDeleteModalConfirm = () => {
        dispatch(projectActions.deleteLocalization(confirmDelete.id));
        setConfirmDelete({
            showModal: false,
            id: null
        });
    };

    const handleConfirmDeleteModalClose = () => {
        setConfirmDelete({
            showModal: false,
            id: null
        });
    };

    const handleConfirmDeleteUserModalConfirm = () => {
        dispatch(userActions.deleteUser({user_id:confirmUserDelete.id,project_id:params.project_id}));
        setConfirmUserDelete({
            showUserModal: false,
            id: null
        });
    };

    const handleConfirmDeleteUserModalClose = () => {
        setConfirmUserDelete({
            showUserModal: false,
            id: null
        });

    };

    const {handleSubmit, register, errors} = useForm({
        validationSchema: ProjectSettingsValidation

    });
    //
    const onSubmit = (values, e) => {
        dispatch(projectActions.addnewLanguageRequest({...values, project_id: params.project_id}));
        e.target.reset();
        console.log(values);
        handleAddModalClose();
    };


    const renderFormModal = () => (
        <Modal
            title="Add new Languange"
            show={showAddModal}
            onClose={handleAddModalClose}
        >


            {/* Modal Body */}
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-wrapper">
                    <label htmlFor="key">Languange Name</label>
                    <input
                        name="name"
                        id="name"
                        ref={register}
                    />
                    {errors.name && errors.name.message}

                </div>
                <div className="form-wrapper">
                    <label htmlFor="key">Abrevation</label>
                    <input
                        name="abbreviation"
                        id="abbreviation"
                        ref={register}
                    />
                    {errors.abbreviation && errors.abbreviation.message}

                </div>
                <div className="button-part">
                    <button
                        className="btn text-muted"
                        onClick={handleAddModalClose}
                    >
                        Close
                    </button>
                    <button type="submit" className="base-button">Add new Language</button>
                </div>
            </form>
        </Modal>
    );

    const renderEditFormModal = () => (
        <ProjectEditForm
            currentEditing={props.currentEditing}

            handleEditModalClose={handleEditModalClose}
            onSubmit={(data) => {
                dispatch(projectActions.editLocalizationValue({
                    ...data,
                    project_id: params.project_id
                }));
                console.log(data);
                dispatch(
                    projectActions.setCurrentlyEditing(null),
                );
                setshowEditModal(false);
            }}
        />
    );

    const renderInviteUserModal = () => (
        <InviteUser
            handleInviteUserModalClose={handleInviteUserModalClose}
            handleInputChange={handleInputChange}
            suggestedUsers={suggestedUsers}
            handleUserSelect={handleUserSelect}
            handleInviteUser={handleInviteUser}
        />
    );


    const renderContent = () => (

        <ConfirmDeleteModal
            show={confirmDelete.showModal}
            title="Delete"
            message="Do you really want to delete these Language? This process cannot be undone."
            onConfirm={handleConfirmDeleteModalConfirm}
            onClose={handleConfirmDeleteModalClose}
        />

    );

    const renderDeleteUserContent = () => (

        <ConfirmDeleteModal
            show={confirmUserDelete.showUserModal}
            title="Delete"
            message="Do you really want to delete these User? This process cannot be undone."
            onConfirm={handleConfirmDeleteUserModalConfirm}
            onClose={handleConfirmDeleteUserModalClose}
        />

    );

    return (
        <>
            <div className="container settings">

                <div className="content-head  -margin-top-content">
                    <span>
                        Projects /
                        {project.name} /
                        Settings
                    </span>
                    {!project.name && <h4>Loading ...</h4>}
                    <h4>{project.name}</h4>
                    <p className="three-dots" ref={node} onClick={e => setState(!state)}>
                        <p> ... </p>
                    </p>
                </div>

                {state ? <Tooltip onClick={handleProjectDelete}/> : <span></span>}
                <Tabs
                    className=""
                >

                    <TabPanel
                        className=""
                        title="Languanges"
                    >

                        <div className="row justify-content-md-center main-content">
                            <div className="col-md-12 row-centered ">
                                <div className="localization-head">
                                    {
                                        project.localizations.length === 1 && (
                                            <div>
                                                <h5>Languanges</h5>

                                                <div className="add-other-language" onClick={handleAddClick}>
                                                    <img src={CircleLogo} alt=""/>
                                                    Add new Languange
                                                </div>
                                            </div>
                                        )
                                    }

                                </div>


                                <div className="main-content">
                                    {!props.loading && project.localizations.length === 0 && (
                                        <div className="card  content-card">
                                            <div className="content">
                                                <img src={Translation} alt=""/>
                                                <h5>Localization</h5>
                                                <p>To add a new localization to your project, click button below</p>
                                                <button
                                                    type="submit"
                                                    className="base-button"
                                                    onClick={handleAddClick}
                                                >
                                                    + Add
                                                    new
                                                    Project
                                                </button>
                                            </div>
                                        </div>
                                    )}

                                    {props.loading && <ComponentSpinner/>}

                                    {!props.loading && project.localizations.length > 0 && (
                                        <div className="">
                                            <ProjectSettingsItem
                                                onEdit={handleOnEdit}
                                                onDelete={handleOnDelete}
                                                project={project}
                                            />
                                        </div>
                                    )}
                                </div>


                            </div>
                        </div>
                    </TabPanel>
                    <TabPanel
                        className=""
                        title="Users"
                    >
                        <div className="row justify-content-md-center main-content">
                            <div className="col-md-12 row-centered ">
                                <div className="localization-head">
                                    <h5>Settings</h5>
                                    <div className="add-other-language" onClick={handleInviteUserModalClick}>
                                        <img src={CircleLogo} alt=""/>
                                        Add another user
                                    </div>
                                </div>


                                {!props.loading && !projectUser && (
                                    <div className="card  content-card">
                                        <div className="content">
                                            <img src={Translation} alt=""/>
                                            <h5>Localization</h5>
                                            <p>To add a new localization to your project, click button below</p>
                                            <button
                                                type="submit"
                                                className="base-button"
                                                onClick={handleAddClick}
                                            >
                                                + Add
                                                new
                                                User
                                            </button>
                                        </div>
                                    </div>
                                )}
                                {props.loading && <ComponentSpinner/>}

                                {!props.loading && projectUser && (
                                    <div className="">
                                        <ProjectSettingUser
                                            onEdit={handleOnEdit}
                                            onUserDelete={handleOnUserDelete}
                                            handleAddClick={handleOnUserDelete}
                                            projectUser={projectUser}
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </TabPanel>

                </Tabs>
            </div>
            {renderFormModal()}
            {renderDeleteUserContent()}
            {renderContent()}

            {showEditModal && renderEditFormModal(props)}
            {showInviteUserModal && renderInviteUserModal(props)}
        </>

    );
}

const mapStateToProps = (state) => ({
    project: state.app.project.item,
    loading: state.app.project.loading,
    currentEditing: state.app.project.currentEditing,
    projectUser: state.app.projectUser.item,
    error: state.app.projectUser.error,
    suggestedUsers: state.app.projectUser.users.users,
});

export default connect(mapStateToProps)(ProjectSettingsPage);
