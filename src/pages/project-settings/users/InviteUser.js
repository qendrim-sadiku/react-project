import React, { useState } from 'react';
import useForm from 'react-hook-form';
import Modal from 'components/UI/Modals/Modal';
import * as yup from 'yup';
import PropTypes from 'prop-types';
import Tabs from 'components/Tabs';
import Checked from 'static/icons/checked.svg'

function InviteUser({
                        handleInviteUserModalClose,
                        handleInputChange,
                        onSubmit,
                        suggestedUsers,
                        handleUserSelect
                    }) {
    const InviteUserFormValidation = yup.object().shape({
        email: yup.string().required(' is Required'),
    });

    const { handleSubmit, register, errors } = useForm({
        validationSchema: InviteUserFormValidation
    });

    const [state, setState] = useState(true);


    function toggle() {
        state ? setState(false) : setState(true);
    }
    return (
        <>
            <Modal
                title="Invite User"
                show
                onClose={handleInviteUserModalClose}
            >

                <form onSubmit={handleSubmit((values) => {
                    // onSubmit({ project_id, user_id });
                    console.log(values);
                })}
                >
                    <div className="form-wrapper">
                        <label htmlFor="value">Email</label>
                        <input
                            id="email"
                            name="email"
                            ref={register}
                            onChange={handleInputChange}
                        />
                        {errors.email && errors.email.message}
                    </div>
                    <div className="button-part">
                        <button
                            className="btn text-muted"
                            onClick={handleInviteUserModalClose}
                        >
                            Close
                        </button>
                        <button type="submit" className="base-button">Invite User</button>
                    </div>
                </form>
                <div>
                    {suggestedUsers
                    && suggestedUsers.map((item) => (
                        <div className="users" onClick={handleUserSelect}>
                            <p>{item.email}</p>
                        </div>
                    ))
                    }
                </div>
            </Modal>
        </>
    );
}

InviteUser.propTypes = {
    handleInviteUserModalClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};


export default InviteUser;
