import React, { useState, useEffect } from 'react';
import {actions as userActions} from "sagas/user/user";
import {connect} from "react-redux";
import queryString from 'query-string/index'

function UserJoin(props) {
    const {
        dispatch,
    } = props;


    const parsed = queryString.parse(props.location.search);

    console.log(parsed);

    useEffect(() => {
        // request data first load
        dispatch(
            userActions.userJoin(parsed)
        );
    }, [dispatch,parsed]);


    return (
        <div>
            Joined
        </div>
    )
}

const mapStateToProps = (state) => ({
    user: state.app.user.items,
});

export default connect(mapStateToProps)(UserJoin);
