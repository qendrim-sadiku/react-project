import React from 'react';
import useForm from 'react-hook-form';
import Modal from 'components/UI/Modals/Modal';
import * as yup from 'yup';
import PropTypes from 'prop-types';

function ProjectEditForm({
    currentEditing,
    handleEditModalClose,
    onSubmit
}) {
    const EditFormValidation = yup.object().shape({
        name: yup.string().required('Value is Required'),
        abbreviation: yup.string().required('Value is Required'),
    });

    const { handleSubmit, register, errors } = useForm({
        validationSchema: EditFormValidation
    });

    return (
        <>
            <Modal
                title="Edit Languange"
                show
                onClose={handleEditModalClose}
            >
                {currentEditing
                && (
                <form onSubmit={handleSubmit((values) => {
                    const { id } = currentEditing;
                    const { name ,abbreviation} = values;
                    onSubmit({ name,abbreviation, id });
                    console.log(values);
                })}
                >
                    <div className="form-wrapper">
                        <label htmlFor="value">Name</label>
                        <input
                            id="name"
                            name="name"
                            ref={register}
                            defaultValue={currentEditing.name}
                        />
                         {errors.name && errors.name.message}
                    </div>
                    <div className="form-wrapper">
                        <label htmlFor="value">Abrevation</label>
                        <input
                            id="abbreviation"
                            name="abbreviation"
                            ref={register}
                            defaultValue={currentEditing.abbreviation}
                        />
                         {errors.abbreviation && errors.abbreviation.message}
                    </div>
                    <div className="button-part">
                        <button
                            className="btn text-muted"
                            onClick={handleEditModalClose}
                        >
                                Close
                        </button>
                        <button type="submit" className="base-button">Edit Language</button>
                    </div>
                </form>
                )}
            </Modal>
        </>
    );
}

ProjectEditForm.propTypes = {
    currentEditing: PropTypes.object.isRequired,
    handleEditModalClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};


export default ProjectEditForm;
