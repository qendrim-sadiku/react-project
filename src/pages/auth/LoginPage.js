import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import useForm from 'react-hook-form';
import {actions as userActions} from 'sagas/auth/userSaga';
import * as yup from "yup";
import PropTypes from 'prop-types';

const SigninSchema = yup.object().shape({
    email: yup.string().email().required("Email is required"),
    password: yup.string().required("Password is required"),
});

function LoginPage(props) {
    // if loged in redirect to dashboard

    const {
        logedIn,
        history,
        formStatus
    } = props;

    useEffect(() => {
        if (logedIn) {
            history.push('/projects/list');
        }
        if (!logedIn) {
            history.push('/auth/login');
        }
    }, [history, logedIn]);




    const {register, handleSubmit, errors} = useForm({
        validationSchema: SigninSchema
    });
    console.log();
    const onSubmit = values => {
        props.dispatch(userActions.loginRequest(values));

    };

    return (
        <div className="container -margin-top-content">

            <div className="row justify-content-md-center">
                <div className="col-md-6 row-centered ">
                    <h4>Login</h4>
                    <div className="card login-card">
                        <div className="register-content">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="form-wrapper">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        name="email"
                                        id="email"
                                        ref={register({
                                            required: 'Required',
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                message: 'Invalid email address'
                                            }
                                        })}
                                    />
                                </div>
                                {errors.email && errors.email.message}

                                <div className="form-wrapper">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        ref={register({
                                            required: 'Password is required',
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                // message: "Password is required"
                                            }
                                        })}
                                    />
                                </div>
                                {errors.password && errors.password.message}

                                <button type="submit" className="base-button">Login</button>
                                {
                                    formStatus && formStatus.message
                                    && (
                                        <div className="alert alert-danger" role="alert">
                                            {formStatus.message}
                                        </div>
                                    )
                                }
                            </form>

                        </div>
                    </div>
                    <Link to="/auth/register" className="Info-text">
                        New User ?
                        <span> Create an account.</span>
                    </Link>
                </div>

            </div>

        </div>
    );
}

LoginPage.propTypes = {
    logedIn: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    logedIn: state.app.user.logedIn,
    error: state.app.user.error,
    formStatus: state.app.user.LoginformStatus
});

export default connect(mapStateToProps)(LoginPage);
