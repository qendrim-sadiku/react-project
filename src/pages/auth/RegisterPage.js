import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import useForm from 'react-hook-form';
import {actions as userActions} from 'sagas/auth/userSaga';
import * as yup from 'yup';
import PropTypes from 'prop-types';

const SignupSchema = yup.object().shape({
    email: yup.string().email().required('Email is required'),
    password: yup.string().min(8).required('Password is required'),
    confirmPassword: yup.string()
        .oneOf( [ yup.ref('password'), null], 'Passwords must match')
});


function RegisterPage(props) {

    const {
        logedIn,
        history,
        formStatus
    } = props;

    useEffect(() => {
        if (logedIn) {
            history.push('/projects/list');
        }
    }, [history, logedIn]);

    const {register, handleSubmit, errors} = useForm({
        validationSchema: SignupSchema
    });
    const onSubmit = values => {
        props.dispatch(userActions.registerRequest(values));

    };

    return (
        <div className="container -margin-top-content">

            <div className="row justify-content-md-center">
                <div className="col-md-6 row-centered ">
                    <h4>Register</h4>
                    <div className="card login-card">
                        <div className="register-content">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="form-wrapper">
                                    <label htmlFor="name">Email</label>
                                    <input
                                        name="email"
                                        id="email"
                                        ref={register({})}
                                    />
                                    {/* {errors.email && errors.email.message} */}
                                </div>
                                <div className="form-wrapper">
                                    <label htmlFor="name">first name</label>
                                    <input
                                        name="first_name"
                                        id="first_name"
                                        ref={register({})}
                                    />
                                    {/* {errors.email && errors.email.message} */}
                                </div>
                                <div className="form-wrapper">
                                    <label htmlFor="name">last name</label>
                                    <input
                                        name="last_name"
                                        id="last_name"
                                        ref={register({})}
                                    />
                                    {/* {errors.email && errors.email.message} */}
                                </div>
                                <div className="form-wrapper">
                                    <label htmlFor="name">last name</label>
                                    <input
                                        name="date_of_birth"
                                        id="date_of_birth"
                                        ref={register({})}
                                    />
                                    {/* {errors.email && errors.email.message} */}
                                </div>
                                <div className="form-wrapper">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        ref={register({})}
                                    />
                                    {errors.password && errors.password.message}
                                </div>

                                <button type="submit" className="base-button">Register</button>
                                {
                                    formStatus && formStatus.message
                                    && (
                                        <div className="alert alert-danger" role="alert">
                                            { formStatus.message}
                                        </div>
                                    )
                                }
                            </form>
                        </div>
                    </div>
                    <Link to="/auth/login" className="Info-text">
                        Already registered? <span> Login.</span>
                    </Link>
                </div>

            </div>

        </div>
    );
}

RegisterPage.propTypes = {
    logedIn: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    registerd: state.app.user.registerd,
    logedIn: state.app.user.logedIn,
    formStatus: state.app.user.LoginformStatus
});


export default connect(mapStateToProps)(RegisterPage);
