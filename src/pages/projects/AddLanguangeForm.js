import React from 'react';
import useForm from 'react-hook-form';
import * as yup from 'yup';
import PropTypes from 'prop-types';

function AddLanguageForm({
         onSubmit
     }) {
    const AddFormValidation = yup.object().shape({
        value: yup.string().required('Value is Required'),
    });

    const {handleSubmit, register, errors} = useForm({
        validationSchema: AddFormValidation
    });

    return (
        <>
            <div>
                <form onSubmit={handleSubmit((values) => {
                    // const { id } = currentEditing;
                    // const { value } = values;
                    onSubmit();
                })}
                >
                    <div className="form-wrapper">
                        <label htmlFor="value">Language</label>
                        <input
                            id="value"
                            name="value"
                            ref={register}
                        />
                        {errors.value && errors.value.message}
                    </div>
                    <div className="">
                        <button type="submit" className="">Add new Projcet</button>
                    </div>
                </form>
            </div>
        </>
    );
}

AddLanguageForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};


export default AddLanguageForm;
