import React from 'react';
import Folder from 'static/icons/folder-19.svg';
import { Link } from 'react-router-dom';
import RightArrow from 'static/icons/arrow-bottom-right.svg';
import PropTypes from 'prop-types';

function ProjectItem(props) {
    return (
        <div className="content">

            <div className="left-part">
                <img src={Folder} alt="" />
                <Link to={props.to}>{props.name}</Link>
            </div>
            <div className="right-part">
                <img src={RightArrow} alt="" />
            </div>
        </div>
    );

}

ProjectItem.propTypes = {
    name: PropTypes.string.isRequired,
};

export default ProjectItem;
