import React, { useState, useEffect } from 'react';
import FolderLogo from 'static/icons/folder-edit.svg';
import Modal from 'components/UI/Modals/Modal';
import CircleLogo from 'static/icons/circle-add.svg';
import Addicon from 'static/icons/small-add.svg';
import ProjectItem from 'pages/projects/ProjectItems';
import * as yup from "yup";
import useForm from 'react-hook-form';
/**
 * Actions
 */
import { actions as projectActions } from 'sagas/projects/projects';
import {actions as userActions} from "sagas/user/user";
import { connect } from 'react-redux';
import ComponentSpinner from 'components/UI/ComponentSpinner';

function createArrayWithNumbers(length) {
    return Array.from({ length }, (_, k) => k + 1);
}

function ProjectsPage(props) {
    const {
        dispatch,
        projects,
        localizations,
    } = props;

    const [size, setSize] = useState(1);

    const {params} = props.match;

    useEffect(() => {
        // request data first load
        dispatch(
            projectActions.request()
        );
    }, [dispatch,params]);


    /**
     * Sate
     */
    const [showAddModal, setShowAddModal] = useState(false);

    /**
     * Handlers
     */
    const handleAddClick = () => {
        setShowAddModal(true);
    };

    const handleAddModalClose = () => {
        setShowAddModal(false);
    };

    const { handleSubmit, register, errors } = useForm({});
    //
    const onSubmit = (values, e) => {
        const data = {name:values.name};
        data.languages = values.languageName.map( x => ({ name: x })).filter(x => x.name);
        props.dispatch(projectActions.addnewProjectRequest(data));
        e.target.reset();
        handleAddModalClose();
    };

    const renderFormModal = () => (
        <Modal
            title="Add new project"
            show={showAddModal}
            onClose={handleAddModalClose}
        >


            {/* Modal Body */}
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-wrapper">
                    <label>Project Name</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        ref={register({ required: 'Project name is required!' })}
                    />
                    {errors.name && errors.name.message}
                </div>
                {createArrayWithNumbers(size).map((name) => {
                    return (
                        <div key={name}>
                            <div className="form-wrapper">
                                <label htmlFor="firstName">Project Language</label>
                                <input
                                    name={`languageName[${name}]`}
                                    placeholder=""
                                    ref={register({ required: 'Localization name is required!' })}
                                />
                                {errors[`languageName[${name}]`] && errors[`languageName[${name}]`].message}
                            </div>
                        </div>
                    );
                })}
                 <div className="add-other-language" onClick={() => setSize(size + 1)}>
                    <img src={CircleLogo} alt=""/>
                    Add another Language
                 </div>
                <div className="button-part">
                    <button
                        className="btn text-muted"
                        onClick={handleAddModalClose}
                    >
                        Close
                    </button>
                    <button type="submit" className="base-button">Create Projcet</button>
                </div>
            </form>

        </Modal>
    );

    return (
        <>
            <div className="container -margin-top-content">
                <div className="row justify-content-md-center">
                    <div className="col-md-12 row-centered ">
                        <div className="content-head">
                            <h4>Projects</h4>
                            {props.projects && (
                                <div className="transparent-button project-page-button"  onClick={handleAddClick}>
                                <img src={Addicon} alt="" />
                                <p>  Add new project </p>
                                </div>
                            )}
                        </div>

                        {!props.loading && projects.length === 0 && (
                            <div className="card content-card">
                                <div className="content">
                                    <img src={FolderLogo} alt="" />
                                    <h5>Create your first project</h5>
                                    <button type="submit" className="base-button" onClick={handleAddClick}>
                                        + Add new
                                        Project
                                    </button>
                                </div>
                            </div>
                        )}
                        {props.loading && <ComponentSpinner />}

                        {props.projects && (
                            <div className="project-list">
                                {
                                    projects.map((item) => {
                                        const defaultLocalizationId = item.localizations && item.localizations.length && item.localizations[0].id;
                                        return <ProjectItem to={`/projects/${item.id}/localization/${defaultLocalizationId}`} key={item.id} name={item.name} />;
                                    })
                                }
                            </div>
                        )}

                    </div>
                </div>
            </div>
            {renderFormModal()}
        </>
    );
}

const mapStateToProps = (state) => ({
    projects: state.app.projects.items,
    localizations: state.app.localizations,
    loading: state.app.projects.loading
});

export default connect(mapStateToProps)(ProjectsPage);
